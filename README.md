# uboot-pinebookpro

Uboot for Pinebook Pro

## Build `idbloader.img`

```bash
git clone https://github.com/rockchip-linux/rkbin.git
rkbin/tools/mkimage -n rk3399 -T rksd -d rk3399_ddr_800MHz_v1.15.109.bin idbloader.img
cat rkbin/bin/rk33/rk3399_miniloader_v1.26.bin >> idbloader.img
```

## Build `trust.img`

```bash
git clone https://github.com/rockchip-linux/rkbin.git
cd rkbin/
tools/trust_merger RKTRUST/RK3399TRUST.ini
mv trust.img ../
```

## Build `u-boot-dtb.bin`

```bash
export CROSS_COMPILE="ccache aarch64-linux-gnu-"
# silence: disk/part_efi.c:858:49: error: taking address of packed member of 'struct _gpt_entry' may result in an unaligned pointer value [-Werror=address-of-packed-member]
export KCFLAGS="-Wno-error=address-of-packed-member"

git clone https://github.com/mrfixit2001/rockchip-u-boot.git
git -C rockchip-u-boot checkout 183e247
cat patches/*.patch | git -C rockchip-u-boot am
make -C rockchip-u-boot pinebookpro-rk3399_defconfig
make -C rockchip-u-boot all -j$(nproc)
```

## Build `uboot.img` from `u-boot-dtb.bin`

```bash
git clone https://github.com/rockchip-linux/rkbin.git
rkbin/tools/loaderimage --pack --uboot rockchip-u-boot/u-boot-dtb.bin uboot.img 0x200000
```
